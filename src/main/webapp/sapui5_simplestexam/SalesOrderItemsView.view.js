sap.ui.jsview("sapui5_simplestexam.SalesOrderItemsView", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.SalesOrderItemsView
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.SalesOrderItemsView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.SalesOrderItemsView
	*/ 
	createContent : function(oController) {
		var oPanel = new sap.ui.commons.Panel();
		var oTable = new sap.ui.table.Table({editable:true, width: "60%"});

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "BusinessPartnerID"}),
		  template: new sap.ui.commons.TextView().bindProperty("text", "SalesOrderID"),
		  sortProperty: "carrid"
		}));

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "EmailAddress"}),
		  template: new sap.ui.commons.TextField().bindProperty("value", "ProductName"),
		  sortProperty: "connid",
		}));


		oTable.bindRows({path:"/SalesOrders/SalesOrderItems"});

		oPanel.addContent(oTable);
		
		return oPanel;
	}

});
